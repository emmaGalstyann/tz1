jQuery(function ($) {

    $("#phone-number").mask("+38(099) 999-99-99", { placeholder: "+38(0__) ___-__-__" });

    $('#login-form').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
                minlength: 4
            }
        },
        messages: {
            email: {
                email: 'Некорректный формат',
                required: "Обязательное поле",
            },
            password: {
                minlength: 'Введите минимум 4 символа',
                required: "Обязательное поле",
            }
        },
    });
    $('#login-form input').bind('keyup blur click', function () {
        if ($('#login-form').validate().checkForm()) {
            $('.login-validation').prop('disabled', false);
        } else {
            $('.login-validation').prop('disabled', true);
        }
    });
    $('#search-form').validate({
        rules: {
            search: {
                required: true,
                minlength: 2
            },
        },
    })

    var filterForm = $('#filter-form').validate({
        rules: {
            rangeMin: {
                min: 0
            },
            rangeMax: {
                min: 0
            },
        },
    });
    filterForm.resetForm();

    $('.clear').click(function (e) {
        e.preventDefault();
        $('#filter-form select').each(function () {
            $(this).val($(this).find("option:first").val())
        })
        $('#filter-form input:not([type="submit"])').each(function () {
            $(this).val('')
        })
    });

    var subscribeForm = $('#subscribe-form');
    subscribeForm.validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
        },
    })
    $('#subscribe-form .btn').on('click', function () {
        if (subscribeForm.valid()) {
            $('.modal-wrapper.subscribe').addClass('active');
        }
    })



    $('.links > div').on('click', function () {
        $('.links > div').removeClass('active');
        $(this).addClass('active')
    })


    $('.modal-open').on('click', function () {
        var attrPopUp = $(this).attr('data-popUp');
        $('.modal-wrapper').each(function () {
            var attrName = $(this).attr('data-name');
            if (attrPopUp == attrName) {
                $(this).addClass('active')
            }
        })
    })
    $('.modal-wrapper .close').on('click', function () {
        $('.modal-wrapper.active').removeClass('active')
    })


    $(".add-to-cart, .compare, .favorites").on('click', function (e) {
        e.preventDefault();
        counter($(this));
    });
    function counter(el) {
        var el = el.attr('data-btn-name'),
            compare = $('header .right-part').find('.' + el).find('.counter'),
            counter = compare.text();
        compare.text(++counter);
        if (counter > 0) {
            compare.addClass('visibility')
        }
    }

    if ($(window).width() < 767) {
        $('.products').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            //autoplay: true,
            //autoplaySpeed: 5000,
            cssEase: 'linear',
            arrows: false,
            dots: true
        });
    }

    $('header .menu-icon').on('click', function () {
        $("header .main-menu").slideToggle()
    });
    $('.menu-header .menu-icon').on('click', function () {
        $(".row-content .menu ul").slideToggle()
    })



})

